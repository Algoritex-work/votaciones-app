<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//Route::get('datatables/reservations', ['as' => 'admin.reservations.datatable', 'uses' => 'ReservationController@datatable']);



// VOTACIONES
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'VotesController@index');
    Route::get('home', 'VotesController@index');
    Route::get('votaciones', 'VotesController@index');
// get vote
    Route::get('/votar/{id_vote}', ['as' => 'edit', 'uses' => 'VotesController@edit']);
// post vote
    Route::post('enviar', 'VotesController@postVote');

    Route::get('ver-votacion/{id_vote}', 'VotesController@show');
});

Route::group(['middleware' => 'admin'], function () {
    Route::resource('admin', 'AdminController');
    Route::get('admin.datatable', 'AdminController@getDataVotes');
    Route::get('admin.delete_vote/{id_vote}', 'AdminController@destroy_vote');
    Route::get('admin/finish/{id_vote}', 'AdminController@finishVote');
    Route::get('admin/publish/{id_vote}', 'AdminController@publish');
});
