<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableResultadoVotaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('votes_result', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_vote')->unsigned();
            $table->string('user_vote_list', 1000);
            $table->string('winner_list');
            $table->smallInteger('percentage');
            $table->integer('total_votes');
            $table->integer('total_users');
            $table->timestamps();
            $table->foreign('id_vote')->references('id')->on('votes');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('votes_result');
    }
}
