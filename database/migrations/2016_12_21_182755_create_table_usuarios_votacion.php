<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsuariosVotacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_vote')->unsigned();
            $table->string('id_user');
            $table->string('user_vote_list', 2500);
            $table->timestamps();

            $table->foreign('id_vote')->references('id')->on('votes');
            $table->foreign('id_user')->references('email')->on('users_tbl');
            $table->index(['id_vote', 'id_user']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users_votes');
    }
}
