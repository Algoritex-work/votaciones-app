var votes_datatable;

jQuery(document).ready(function() {

    // CREATION OF THE TABLE
    votes_datatable = jQuery('#table_votes').DataTable({
        pagingType: "simple",
        responsive: true,
        "iDisplayLength": 50,
        "bInfo": false,
        "searching": false,
        "bFilter" : false,
        "bLengthChange": false,
        "order": [[ 0, "desc" ]],
        ajax: {
            url: "admin.datatable",
            dataSrc: ''
        },
        "language": {
            "buttons": {
                "pageLength": {
                    "_": "Mostrar %d filas"
                }
            },

            "lengthMenu": "Mostrar _MENU_ filas",
            "emptyTable":     "No hay ninguna votación en el sistema",
            "infoEmpty":      "Mostrando 0 de 0 de un total de 0 entradas",
            "loadingRecords": "Cargando...",
            "search":         "Buscar:",
            "zeroRecords":    "No hay ninguna votación en el sistema",
            "paginate": {
                "first":      "Primero",
                "last":       "&Uacute;ltimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        },

        columns: [
            {
                "data": "id",
            },
            {
                "data": "vote_name"
            },
            {
                "data": "status"
            },
            {
                "data": "start_date"
            },
            {
                "data": "end_date"
            },
            {
                "data": "number_of_winners"
            },
            {
                "data": "votes_per_user"
            },
            {
                "data": "activated"
            }
        ],
        columnDefs: [
            {
                "targets": [0], visible: false
            },
            {
                responsivePriority: 1, targets: 1
            },
            {
                responsivePriority: 2, targets: -1
            },
            {
                responsivePriority: 3, targets: 2
            },
            {
                responsivePriority: 4, targets: 3
            },
            {
                responsivePriority: 5, targets: 4
            },
            {
                "targets": 2,
                "render": function (data) {
                    switch (data) {
                        case 0: return "<span class='label label-default'> Pendiente</span>";
                                    break;
                        case 1: return "<span class='label label-success'> Activa</span>";
                            break;
                        case 2: return "<span class='label label-info'>Terminada</span>";
                            break;
                        default: return "<span class='label label-default'>Pendiente</span>";
                            break;
                    }
                }
            },
            {
                "targets": 7,
                "render": function (data, type, row) {
                    var token = $('meta[name="csrf-token"]').attr('content');
                    if ( row['status'] == 0 ){
                        return "<a class='btn btn-xs btn-warning' href='admin/publish/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Iniciar votación'><i class='fa fa-bell'></i> Iniciar</a>"
                            + "<a onclick='return confirmDelete()' class='btn btn-xs btn-danger' href='admin.delete_vote/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Borrar votación'><i class='fa fa-remove'></i> Eliminar</a>";
                    }
                    else if ( row['status'] == 1 ) {
                        return "<a class='btn btn-xs btn-warning' href='admin/finish/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Terminar y publicar votación'><i class='fa fa-lock'></i> Terminar</a>"
                            + "<a class='btn btn-xs btn-info' href='admin/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Ver resultados de la votación'><i class='fa fa-eye'></i> Ver resultado</a>"
                            + "<a  onclick='return confirmDelete()' class='btn btn-xs btn-danger' href='admin.delete_vote/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Borrar votación'><i class='fa fa-remove'></i> Eliminar</a>";
                    }
                    else if ( row['status'] == 2 ) {
                        return "<a class='btn btn-xs btn-info' href='admin/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Ver resultados de la votación'><i class='fa fa-eye'></i> Ver resultado</a>"
                            + "<a onclick='return confirmDelete()' class='btn btn-xs btn-danger' href='admin.delete_vote/" + row['id'] + "'" + " data-toggle='tooltip' data-placement='bottom' title='Borrar votación'><i class='fa fa-remove'></i> Eliminar</a>";
                    }
                        /*
                        "<form method='post' class='form_delete' action='admin.delete_vote'>" +
                        "<input type='hidden' name='_token' value=" + token +  ">" +
                        "<input type='hidden' name='id_vote' value='" + row['id'] + "'/>" +
                        "</form></a>";
                        */
                }
            }
        ]
    });


// EDITION
    function updateRow(votes_datatable, dataRow, nRow)
    {
        votes_datatable.row(nRow).data(dataRow.data[0])
            .draw(true);
    }


    $(document).on('click', "#create_vote", function(event){
        event.preventDefault();
        window.location.href = "admin/create";
    });


    $(document).on('click', ".remove_vote", function(event){
        event.preventDefault();
        $('.form_delete').submit();
    });

});


function confirmDelete() {
    return confirm("Estas seguro de que quieres elminar esta votación?")
}