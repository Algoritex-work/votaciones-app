<div class="container">

    <header class="row vertical-align">
        <div class="col-sm-4">
            <a href="/">
                <img class="small_logo" src="{{ url('images/logo_small.png') }}"/>
            </a>
        </div>

        <div class="col-sm-2">
            <h3> Elecciones Oepb </h3>
        </div>

        <div class="col-sm-6">

            <ul class="nav panel_toolbox custom_tool_box">
                <li role="presentation" class="active"><a href="{!! url('/') !!}">Votaciones </a></li>
                @if ( Auth::user()->isAdmin())
                    <li role="presentation" class="active"><a href="{!! url('admin') !!}"> Administrar</a>  </li>
                @endif
                <li>&nbsp;&nbsp;&nbsp;</li>
                <li role="presentation" class="active">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="fa fa-user"></span>
                        {!!Auth::user()->username!!}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{!! url('logout') !!}"> <span class="glyphicon glyphicon-log-out"></span> Cerrar sesión </a></li>
                    </ul>
                </li>
            </ul>
        </div>



    </header>

</div>

<br>

