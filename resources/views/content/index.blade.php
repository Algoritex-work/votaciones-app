@extends('master')

@section('content')

    <div class="container" id="custom_layout">

        @if (isset($votaciones['candidates_info']))
        @for( $i=0; $i< count($votaciones['candidates_info']); $i++)
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><i class="fa fa-square-o"></i> {!! $votaciones['vote'][$i]->vote_name !!}</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <ul class="stats-overview">
                                    <li>
                                        <span class="name"> Estado </span>
                                        @if (  $votaciones['vote'][$i]->status == 0)
                                            <span class="value label label-warning">Pendiente</span>
                                        @elseif ( $votaciones['vote'][$i]->status == 1)
                                            <span class="value label label-primary">Activa</span>
                                        @elseif ( $votaciones['vote'][$i]->status == 2)
                                            <span class="value label label-default">Finalizada</span>
                                        @endif
                                    </li>
                                    <li>
                                        <span class="name"><i class="fa fa-calendar"></i> Fecha Inicio </span>
                                        <span class="value text-info"> {!! $votaciones['vote'][$i]->start_date !!} </span>
                                    </li>
                                    <li class="hidden-phone">
                                        <span class="name"><i class="fa fa-calendar-times-o"></i> Fecha Fin </span>
                                        <span class="value text-info"> {!! $votaciones['vote'][$i]->end_date !!} </span>
                                    </li>
                                </ul>
                                <br/>
                                <ul class="stats-overview">
                                    <li>
                                        <span class="name"> Has votado </span>
                                        @if ( $votaciones['voted'][$i])
                                            <span class="value text-success"> <i class="fa fa-check-circle custom_size_icon"></i> </span>
                                        @else
                                            <span class="value text-danger custom_size"> <i class="fa fa-close custom_size_icon"></i> </span>
                                        @endif
                                    </li>
                                    <li>
                                        <span class="name"><i class="fa fa-bullseye"></i> Votos por usuario</span>
                                        <span class="value text-info"> {!! $votaciones['vote'][$i]->votes_per_user !!} </span>
                                    </li>
                                    <li class="hidden-phone">
                                        <span class="name"> <i class="fa fa-trophy"></i> Número de delegados </span>
                                        <span class="value text-info"> {!! $votaciones['vote'][$i]->number_of_winners !!} </span>
                                    </li>
                                </ul>
                            </div>

                            <!-- start project-detail sidebar -->
                            <div class="col-md-3 col-sm-3 col-xs-12">


                                    <h4><i class="fa fa-users"></i> &nbsp;Candidatos</h4>
                                    <p>
                                        @foreach ($votaciones['candidates_info'][$i] as $candidate)
                                            {!! $candidate['full_name'] !!} |
                                        @endforeach
                                    </p>
                                    <br/>

                            </div>
                            <!-- end project-detail sidebar -->
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="text-center mtop20">
                                    @if (  $votaciones['vote'][$i]->status == 0)
                                            <a href="{!! url('votar'). "/". $votaciones['vote'][$i]->id !!}" class="btn btn-primary disabled">Votar</a>
                                            <a href="{!! url('ver-votacion'). "/".  $votaciones['vote'][$i]->id !!}" class="btn btn-success disabled">Ver Resultados</a>
                                   @elseif ( $votaciones['vote'][$i]->status == 1)
                                            @if ( $votaciones['voted'][$i])
                                                <a href="{!! url('votar'). "/". $votaciones['vote'][$i]->id !!}" class="btn  btn-primary disabled">Votar</a>
                                                <a href="{!!  url('ver-votacion'). "/".  $votaciones['vote'][$i]->id  !!}" class="btn btn-success disabled">Ver Resultados</a>
                                             @else
                                                <a href="{!! url('votar'). "/". $votaciones['vote'][$i]->id !!}" class="btn btn-primary">Votar</a>
                                                <a href="{!! url('ver-votacion'). "/".  $votaciones['vote'][$i]->id !!}" class="btn btn-success disabled">Ver Resultados</a>
                                            @endif
                                   @elseif ( $votaciones['vote'][$i]->status == 2)
                                            <a href="{!! url('votar'). "/". $votaciones['vote'][$i]->id !!}" class="btn btn-primary disabled">Votar</a>
                                            <a href="{!!  url('ver-votacion'). "/".  $votaciones['vote'][$i]->id !!}" class="btn  btn-success">Ver Resultados</a>
                                   @endif

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endfor
        @else
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        No hay votaciones creadas
                    </div>
                </div>
            </div>
        @endif

    </div>
@stop

