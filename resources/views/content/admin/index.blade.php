@extends('master')

@section('content')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <div class="container" id="custom_layout">


        <div class="x_panel">
            <div class="x_title">
                <h2><i class="glyphicon glyphicon-bookmark"></i> Administración de votaciones</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- RESULTS AND CANDIDATES -->
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-center">
                        <div class="block">
                            <div class="block-content">
                                <table id='table_votes' class="table table-bordered table-striped" width="100%">
                                    <thead>
                                        <button type="button" class="btn btn-primary" id="create_vote"> <i class="fa fa-plus-square"> &nbsp; Crear una nueva Votación</i></button>
                                    <br>
                                    <tr>
                                        <th> ID </th>
                                        <th> Votación </th>
                                        <th> Estado</th>
                                        <th> Fecha Inicio </th>
                                        <th> Fecha Fin </th>
                                        <th> Ganadores </th>
                                        <th> Votos/Usuario </th>
                                        <th> Acciones </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

