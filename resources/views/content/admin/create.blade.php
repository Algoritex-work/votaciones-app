@extends('master')

@section('content')

<div class="container" id="custom_layout">


        <div class="x_panel">
            <div class="x_title">
                <h2><i class="glyphicon glyphicon-plus-sign"></i> Crear una votación</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <form id="new_vote" name="new_vote" method="post" action="{{url('admin')}}">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 control-label">
                            Nombre Votación
                        </label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" id="name" name="name" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="start_date" class="col-sm-3 control-label">
                            Fecha Inicio
                        </label>
                        <div class="col-sm-2">
                            <div class='input-group date' id='start' data-provide="datepicker" >
                                <input type='text' class="form-control" name="start_date" id="start_date" required/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="end_date" class="col-sm-3 control-label">
                            Fecha Fin
                        </label>
                        <div class="col-sm-2">
                            <div class='input-group date' id='end' data-provide="datepicker">
                                <input type='text' class="form-control" name="end_date" id="end_date" required/>
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="number_user_votes" class="col-sm-3 control-label">
                            Votos por usuario
                        </label>
                        <div class="col-sm-1">
                            <select class="form-control" id="select_number_votes" name="select_number_votes">
                                @for ($i = 1; $i < 51; $i++)
                                    <option>{!! $i !!}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <small> Número de candidatos que podrá elegir cada usuario</small>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="number_user_votes" class="col-sm-3 control-label">
                            Número de elegidos
                        </label>
                        <div class="col-sm-1">
                            <select class="form-control" id="select_number_winners" name="select_number_winners">
                                @for ($i = 1; $i < 51; $i++)
                                    <option>{!! $i !!}</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="file" class="col-sm-3 control-label">
                            Selecciona Candidatos
                        </label>
                        <div class="col-sm-9">
                            <!-- Build your select: -->
                            <select class="form-control" id="candidates_selection" name="candidates_selection[]" multiple="multiple" required>
                                @foreach($users as $user)
                                    <option value="{{$user->email}}">{{$user->full_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div style="text-align: center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> &nbsp; Crear Votación</button>
                    </div>
                </form>
            </div>
        </div>


</div>


<!-- Initialize the plugin: -->
<script type="text/javascript">
    $(document).ready(function() {

        $.fn.datepicker.defaults.language = 'es';

        $('#candidates_selection').multiselect({
            buttonWidth: '400px',
            nonSelectedText: '0 Candidatos seleccionados',
            enableFiltering: true,
            selectAllJustVisible: true,
            maxHeight: 200
        });


        $('#start_date').datepicker({
            locale: 'es',
            format: 'dd/mm/yyyy'
        });


        $('#end_date').datepicker({
             locale: 'es',
             format: 'dd/mm/yyyy'
         });



        jQuery.validator.addMethod("accept", function(value, element, param) {

            var start = new Date($("#start_date").val());
            var end = new Date($("#end_date").val());
            var result = false;
            if (start.getTime() < end.getTime())
                result = true;
            else
                result = false;

            return true;
        });

        var a = $("#new_vote");
        a.validate({
                rules: {
                    start_date: { required: true},
                    end_date: {
                                accept: "#end_date"
                    }
                },
                messages: {
                    end_date: {
                        accept: "La fecha de Fin debe de ser mayor que la fecha de inicio"
                    }
                }
            });

        jQuery.extend(jQuery.validator.messages, {
            required: "Este campo es obligatorio"
        });


    });
</script>
@stop

