@extends('master')

@section('content')
    <div class="container" id="custom_layout">

        <div class="x_panel">
            <div class="x_title">
                <h2><i class="glyphicon glyphicon-bookmark"></i> Votacion: {!! $vote->vote_name  !!}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- RESULTS AND CANDIDATES -->
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">

                        <table class="table table-bordered">
                            <tr>
                                <td>Comienzo: {!! $vote->start_date  !!} </td>
                                <td>Número de delegados  {!! $vote->number_of_winners  !!}</td>
                            </tr>
                            <tr>
                                <td>Finaliza: {!! $vote->end_date  !!} </td>

                                <td>Votos por usuario {!! $vote->votes_per_user  !!}</td>
                            </tr>

                        </table>
                    </div>

                </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <form id="user_vote" name="user_vote" method="post" action="{!! url('enviar') !!}">
                                {{ csrf_field() }}
                                <input type="hidden" name="vote_id" value="{!! $vote->id  !!}">
                                    <div class="">
                                        <ul class="to_do">
                                            @foreach ($candidates as $candidate)
                                            <li>
                                                <p>
                                                    <input type="checkbox" class="flat" id="submit_candidates" name="submit_candidates[]" value="{!! $candidate['email'] !!}">
                                                    {!! $candidate['full_name'] !!}
                                                </p>
                                            </li>
                                            @endforeach
                                        </ul>
                                         <div class="alert alert-danger alert-dismissable" id="error_msg">
                                             Debes seleccionar <strong>{!! $vote->votes_per_user  !!}</strong> candidatos
                                        </div>
                                    </div>

                                <div style="text-align: center">
                                    <button type="submit" class="btn-lg btn-success">Enviar Voto</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="{{ url('gentelella-master/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Initialize the plugin: -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#error_msg').hide();
            function validateCandidates() {
                var limit = "{!! $vote->votes_per_user  !!}";
                var length = $('[name="submit_candidates[]"]:checked').length;
                if( length ==  0 || length != limit)
                    return false;
                else
                    return true;
            }


            /*
            var limit = "{ $vote->votes_per_user  !!}";
            var a = $("#user_vote");
            a.validate({
                rules: {
                    'submit_candidates[]': {
                        required: function () {
                                var length = $('[name="submit_candidates[]"]:checked').length;
                                alert(length);
                                alert(limit);
                                if( length ==  0 || length != limit) {
                                    return true;
                                }
                                else {
                                    return false;
                                 }
                             }
                        },


                },
                messages: {
                    'submit_candidates[]': {
                        required: "Debes seleccionar "+ limit + " candidatos"
                    }
                }
            });

            jQuery.extend(jQuery.validator.messages, {
                required: "Este campo es obligatorio"
            });
        */


            $('#user_vote').submit(function(event){
                event.preventDefault();
                if (validateCandidates())
                        this.submit();
                else
                        $('#error_msg').show();
            });

        });
    </script>
@stop

