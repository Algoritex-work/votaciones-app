@extends('master')

@section('content')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <div class="container" id="custom_layout">

        <div class="x_panel">
            <div class="x_title">
                <h2><i class="glyphicon glyphicon-th-large"></i> Votación {{ $data['vote_name'] }}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">

                    <div class="col-md-6 col-md-offset-3 col-sm- col-xs-12">
                        <ul class="stats-overview">
                            <li>
                                <span class="name"><i class="fa fa-calendar"></i> Fecha Inicio </span>
                                <span class="value text-info"> {!! $data['start_date'] !!} </span>
                            </li>
                            <li class="hidden-phone">
                                <span class="name"><i class="fa fa-calendar-times-o"></i> Fecha Fin </span>
                                <span class="value text-info"> {!! $data['end_date'] !!} </span>
                            </li>
                            <li>
                                <span class="name"><i class="fa fa-pie-chart"></i> Procentaje de votación</span>
                                <span class="value text-info"> {!! $data['percentage'] !!} %</span>
                            </li>
                        </ul>
                        <br/>
                        <ul class="stats-overview">
                            <li>
                                <span class="name"><i class="fa fa-users"></i> Votos totales</span>
                                <span class="value text-info"> {!! $data['total_votes'] !!} </span>
                            </li>
                            <li class="hidden-phone">
                                <span class="name"> <i class="fa fa-trophy"></i> Número de ganadores </span>
                                <span class="value text-info"> {!! $data['number_of_winners'] !!} </span>
                            </li>
                            <li>
                                <span class="name"><i class="fa fa-thumbs-up"></i> Usuarios que han votado</span>
                                <span class="value text-info"> {!! $data['total_users'] !!} </span>
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
        </div>


        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-trophy"></i> Ganadores</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- RESULTS AND CANDIDATES -->
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-2">
                        <table id="winners_table" class="table table-striped">
                            <thead>
                            <div class="text-center">
                                <h4>Ganadores</h4>
                            </div>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach ( $data['winners'] as  $key => $item)
                                <tr>
                                    <td>#{{ $i+1}}</td>
                                    <td>{{ $item['full_name'] }}</td>
                                    <td>{{ $item['num_of_votes'] }} Votos</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-3 col-sm-offset-2">
                        <table id="winners_table" class="table table-striped">
                            <thead>
                            <div class="text-center">
                                <h4>Resultado Total</h4>
                            </div>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @foreach ( $data['candidates'] as  $key => $item)
                                <tr>
                                    <td>#{{ $i}}</td>
                                    <td>{{ $item['full_name'] }}</td>
                                    <td>{{ $item['num_of_votes'] }} Votos</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3 col-sm-offset-2">

                        <canvas id="winner_statistic" width="200" height="100"></canvas>
                        <script type="text/javascript">
                            var ctx = document.getElementById("winner_statistic");
                            // var array_labels = p echo implode(',', $data['chart_labels']); ?>";
                            //var array_labels = " echo $data['chart_labels'][1] ?>";
                            var array_dataset = [];
                            var array_labels = "";
                            array_labels = <?php echo json_encode($data['chart_winners']['full_name']); ?>;
                            array_dataset['data'] = <?php echo json_encode($data['chart_winners']['num_of_votes']); ?>;
                            array_dataset['color'] = <?php echo json_encode($data['chart_winners']['background_color']); ?>;

                            var myChart = new Chart(ctx, {
                                type: 'pie',
                                data: {
                                    labels: array_labels
                                    ,
                                    datasets: [{
                                        label: '# of Votes',
                                        data: array_dataset['data'],
                                        backgroundColor: array_dataset['color']
                                    }]
                                },
                                options: {
                                    title: {
                                        display: false,
                                        text: 'Custom Chart Title',
                                        responsive: true,
                                    }
                                }
                            });
                        </script>
                    </div>
                    <div class="col-sm-3 col-sm-offset-2">
                        <canvas id="candidates_statistic" width="200" height="100"></canvas>
                        <script type="text/javascript">
                            var ctx = document.getElementById("candidates_statistic");
                            var array_dataset = [];
                            var array_labels = "";
                            array_labels = <?php echo json_encode($data['chart_candidates']['full_name']); ?>;
                            array_dataset['data'] = <?php echo json_encode($data['chart_candidates']['num_of_votes']); ?>;
                            array_dataset['color'] = <?php echo json_encode($data['chart_candidates']['background_color']); ?>;

                            var myChart = new Chart(ctx, {
                                type: 'pie',
                                data: {
                                    labels: array_labels
                                    ,
                                    datasets: [{
                                        label: '# of Votes',
                                        data: array_dataset['data'],
                                        backgroundColor: array_dataset['color']
                                    }]
                                },
                                options: {
                                    title: {
                                        display: false,
                                        text: 'Custom Chart Title',
                                        responsive: true
                                    }
                                }
                            });
                        </script>
                        <!-- Usage as a class -->
                    </div>
                    <!-- Usage as a class -->
                </div>

            </div>
        </div>
    </div>

@stop

