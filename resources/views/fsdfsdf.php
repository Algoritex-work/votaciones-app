<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Votaciones App</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/my_css.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('css/bootstrap-datepicker.min.css') }}"/>


    <link rel="stylesheet" href="{{ url('css/bootstrap-multiselect.css') }}"/>

    <!-- Gentelella Theme-->
    <link href="{{ url('gentelella-master/build/css/custom.min.css') }}" rel="stylesheet" />
    <link href="{{ url('gentelella-master/vendors/fontawesome/css/font-awesome.min.cs') }}" rel="stylesheet" />
    <link href="{{ url('gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />


    <script type="text/javascript" src="{{ url('js/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/bootstrap-multiselect.js') }}"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->







</head>

<body>


@include('references.header')
<br>

@yield('content')

<br> <br>
@include('references.footer')


<!-- Bootstrap core JavaScript
================================================== -->



<script type="text/javascript" src="{{ url('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/dataTables.responsive.min.js') }}"></script>

<script type="text/javascript" src="{{ url('js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ url('js/locales/bootstrap-datepicker.es.js') }}"></script>

<!-- Custom Theme Scripts -->
<script type="text/javascript" src="{{ url('gentelella-master/build/js/custom.min.js') }}"></script>
<!-- END Dynamic Table Full -->



<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

</body>
</html>
