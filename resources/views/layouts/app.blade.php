<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Votaciones App</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Gentelella Theme-->
    <link href="{{ url('gentelella-master/build/css/custom.min.css') }}" rel="stylesheet" />
    <!-- iCheck -->
    <link href="{{ url('gentelella-master/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">


    <link href="{{ url('gentelella-master/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ url('css/my_css.css') }}" rel="stylesheet">
</head>

<body>

@yield('content')

</body>
<!-- Bootstrap core JavaScript
================================================== -->
<script type="text/javascript" src="{{ url('js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<!-- Custom Theme Scripts -->
<script type="text/javascript" src="{{ url('gentelella-master/build/js/custom.min.js') }}"></script>
<!-- END Dynamic Table Full -->
</html>

