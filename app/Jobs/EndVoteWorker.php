<?php

namespace App\Jobs;
use App\Notifications\notifyEndVote;

use App\Votes;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class EndVoteWorker implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    var $vote = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($vote)
    {
        //
        $this->vote = $vote;
        Log::debug('constructor end worker');
    }


    private function notifyFinishedVote($vote) {
        $users = User::all();
        foreach ($users as $user)
            $user->notify(new notifyEndVote($vote));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('EndVoteWorker handle...');
        $this->notifyFinishedVote($this->vote);
    }
}
