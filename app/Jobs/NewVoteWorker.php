<?php

namespace App\Jobs;
use App\Notifications\notifyNewVote;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Votes;
use Log;

class NewVoteWorker implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    var $vote = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($vote)
    {
        $this->vote = $vote;
        Log::debug('constructor start worker');
    }


    private function notifyPusblishedVote($vote) {
        $users = User::all();
        foreach ($users as $user)
            $user->notify(new notifyNewVote($vote));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('NewVoteWorker handle...');
        $this->notifyPusblishedVote($this->vote);

    }
}
