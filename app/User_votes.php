<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_votes extends Model
{
    //
    protected $table = 'users_votes';


    public function vote()
    {
        return $this->belongsTo('App\Votes', 'id_vote', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'email', 'id_user');
    }
}
