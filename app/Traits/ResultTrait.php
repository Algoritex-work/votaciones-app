<?php
/**
 * Created by PhpStorm.
 * User: Kike
 * Date: 28/01/2017
 * Time: 18:25
 */

namespace App\Traits;
use App\Jobs\EndVoteWorker;
use App\Jobs\NewVoteWorker;

use App\Votes;
use App\Votes_result;
use App\Http\Requests;
use App\User_votes;
use App\User;
use Carbon\Carbon;
use Log;


trait ResultTrait{

    /*
    private function notifyPusblishedVote($vote) {
        $users = User::all();
        foreach ($users as $user)
            $user->notify(new notifyNewVote($vote));
    }

    private function notifyFinishedVote($vote) {
            $users = User::all();
            foreach ($users as $user)
                $user->notify(new notifyEndVote($vote));
    }
    */

    private function checkIfFStarted($vote)
    {
        if ($vote->status == 0) {
            $date = Carbon::now('Europe/Brussels')->toDateString();
            $now = Carbon::createFromFormat('Y-m-d', $date)->toDateString();
            $start = Carbon::createFromFormat('d/m/Y', $vote->start_date)->toDateString(); // 1975-05-21 22:00:
            Log::debug(" finished dates " . $vote->id . " now: " .$now . " start: " .$start);
            if ($now >= $start)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    private function checkIfFinished($vote)
    {
        if ($vote->status != 2) {
            $date = Carbon::now('Europe/Brussels')->toDateString();
            $now = Carbon::createFromFormat('Y-m-d', $date)->toDateString();
            $end = Carbon::createFromFormat('d/m/Y', $vote->end_date)->toDateString(); // 1975-05-21 22:00:00
            Log::debug(" finished dates " . $vote->id . " now: " .$now . " end: " .$end);
            if ($now >= $end)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }


    public function publish($vote_id) {
        try{
            $vote = Votes::find($vote_id);;
            $vote->status = 1;
            $vote->save();
            $job = (new NewVoteWorker($vote));
            dispatch($job);
            //$this->notifyPusblishedVote($vote);
            return redirect()->back()->with(['message' => "Votación abierta y activada"]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "Se produjo un error al publicar la votación " . $e->getMessage()]);
        }
    }

    public function finishVote($vote_id) {
        try{
            $this->workOutResults($vote_id);

            $vote = Votes::find($vote_id);
            $vote->status = 2;
            $vote->save();
            //$this->notifyFinishedVote($vote);
            dispatch(new EndVoteWorker($vote));
            return redirect()->back()->with(['message' => "Votación cerrada"]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "Se produjo un error finalizando la votación " . $e->getMessage()]);
        }
    }


    public function workOutResults($id_vote)
    {
        //
        try {
            $vote = Votes::where('id', $id_vote)->get()[0];

            $candidates_list = explode(',', $vote['candidates']);
            $num_winners = $vote ['num_of_winners'];

            // PASAMOS A ARRAY KEY
            foreach ($candidates_list as $value) {
                $result[$value] = 0;
            }

            $user_votes = User_votes::where('id_vote', $id_vote)->get();
            $num_rows = count($user_votes);
            foreach ($user_votes as $user_vote) {
                $user_votes_list = explode(',', $user_vote['user_vote_list']);
                foreach ($user_votes_list as $item) {
                    if (array_key_exists($item, $result))
                        $result[$item]++;

                }
            }
            $num_users = User::all()->count();
            $percentage = $this->getPercentage($num_rows, $num_users);
            arsort($result);

            $winners = $this->getWinners($result, $num_winners);
            $winners = http_build_query($winners, '', ';');
            $winners = str_replace("%40", '@', $winners);

            $result = http_build_query($result, '', ';');
            $result = str_replace("%40", '@', $result);

            $votes_result = Votes_result::where('id_vote', $id_vote)->get()->first();
            if (!isset($votes_result))
                $votes_result = new Votes_result();

            $votes_result->id_vote = $id_vote;
            $votes_result->user_vote_list = $result;
            $votes_result->winner_list = $winners;
            $votes_result->percentage = $percentage;
            $votes_result->total_votes = $num_rows;
            $votes_result->total_users = $num_users;
            $votes_result->save();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    private function getPercentage($num_votes, $total_users)
    {
        return ($total_users * $num_votes) / 100;
    }


    private function getWinners($result, $num_winners)
    {
        return array_slice($result, 0, $num_winners - 1);

    }


    protected function getResults($vote_id)
    {


        $votes_result = Votes_result::where('id_vote', $vote_id)->get()->first();
        if (!isset($votes_result)) {
            return view('content.index', ["error", "Nadie ha votado aún, no se pueden ver los resultados"]);
        }

        $votes = Votes::find($vote_id)->first();


        $candidates = $this->listUserToArray($votes_result->user_vote_list);
        $chart_candidates = "";

        foreach ($candidates as $candidate) {
            $chart_candidates['full_name'][] = $candidate['full_name'];
            $chart_candidates['num_of_votes'][] = $candidate['num_of_votes'];
            $chart_candidates['background_color'][] = "#" . dechex(rand(0x000000, 0xFFFFFF));
        }

        $winners = array_slice($candidates, 0, $votes->number_of_winners);

        $chart_winners = "";

        if ( count($chart_candidates['full_name']) < $votes->number_of_winners)
            $votes->number_of_winners = count($chart_candidates['full_name']);
        for ($i = 0; $i < $votes->number_of_winners; $i++) {
            $chart_winners['full_name'][] = $chart_candidates['full_name'][$i];
            $chart_winners['num_of_votes'][] = $chart_candidates['num_of_votes'][$i];
            $chart_winners['background_color'][] = $chart_candidates['background_color'][$i];
        }


        $output = array(
            'id' => $votes->id,
            'vote_name' => $votes->vote_name,
            'start_date' => $votes->start_date,
            'end_date' => $votes->end_date,
            'number_of_winners' => $votes->number_of_winners,
            'votes_per_user' => $votes->votes_per_user,
            'total_users' => $votes_result->total_users,
            'total_votes' => $votes_result->total_votes,
            'candidates' => $candidates,
            'winners' => $winners,
            'percentage' => $votes_result->percentage,
            'chart_candidates' => $chart_candidates,
            'chart_winners' => $chart_winners

        );
        return $output;
    }


    private function listUserToArray($list)
    {

        $explit_list = explode(';', $list);
        $output = array();
        foreach ($explit_list as $item) {

            $item = explode('=', $item);
            $user = User::find($item[0]);
            $output[$item[0]] = ['full_name' => $user['full_name'], "dni" => $user['dni'], "num_of_votes" => $item[1]];
        }

        return $output;
    }
}