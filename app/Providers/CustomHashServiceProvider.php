<?php

namespace App\Providers;

use App\Libraries\Hasher;

use Illuminate\Hashing\HashServiceProvider;

class CustomHashServiceProvider extends HashServiceProvider {

    public function register()
    {
        $this->app->singleton('hash', function() { return new Hasher; });
    }

}