<?php

namespace App\Http\Controllers;

use App\Votes;
use App\Http\Requests;
use App\Traits\ResultTrait;
use Carbon\Carbon;

use Log;


class BootController extends Controller
{

    use ResultTrait;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

    }

    private function checkIfFStarted($vote)
    {
        if ($vote->status == 0) {
            $date = Carbon::now('Europe/Brussels')->toDateString();
            $now = Carbon::createFromFormat('Y-m-d', $date)->toDateString();
            $start = Carbon::createFromFormat('d/m/Y', $vote->start_date)->toDateString(); // 1975-05-21 22:00:
            Log::debug(" finished dates " . $vote->id . " now: " .$now . " start: " .$start);
            if ($now >= $start)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    private function checkIfFinished($vote)
    {
        if ($vote->status != 2) {
            $date = Carbon::now('Europe/Brussels')->toDateString();
            $now = Carbon::createFromFormat('Y-m-d', $date)->toDateString();
            $end = Carbon::createFromFormat('d/m/Y', $vote->end_date)->toDateString(); // 1975-05-21 22:00:00
            Log::debug(" finished dates " . $vote->id . " now: " .$now . " end: " .$end);
            if ($now >= $end)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $votes = Votes::all();
        foreach ($votes as $vote) {
            if ($this->checkIfFStarted($vote)) {
                Log::debug(" >> Comenzada votación: " . $vote->id . " nombre: " .$vote->vote_name);
                $this->publish($vote->id);
            } else {
                Log::debug("No Comenzada: " . $vote->id . " nombre: " .$vote->vote_name);
            }

            if ($this->checkIfFinished($vote)) {
                Log::debug(">> Terminada Votación: " . $vote->id . " nombre: " .$vote->vote_name);
                $this->finishVote($vote->id);
            } else {
                Log::debug("No Terminada: " . $vote->id . " nombre: " .$vote->vote_name);
            }
        }
    }


}
