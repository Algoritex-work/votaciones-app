<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Votes;

use App\User_votes;
use App\Votes_result;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Traits\ResultTrait;

use Carbon\Carbon;


class AdminController extends Controller
{
    use ResultTrait;
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return view('content.admin.index');
        } catch (\Exception $e) {
            echo "Failed index, " . $e->getMessage();
        }
    }


    public function store(Request $request) {

        try {

            $votes = new Votes();
            $votes->vote_name = $request->get('name');
            $votes->start_date = $request->get('start_date');
            $votes->end_date = $request->get('end_date');
            $votes->candidates = $request->get('candidates_selection');
            $votes->votes_per_user = $request->get('select_number_votes');
            $votes->number_of_winners = $request->get('select_number_winners');
            $votes->candidates = implode(',', $request->get('candidates_selection'));
            $votes->status = 0;

            $start = Carbon::createFromFormat('d/m/Y', $votes->start_date)->toDateString();
            $end = Carbon::createFromFormat('d/m/Y', $votes->end_date)->toDateString(); // 1975-05-21 22:00:00

            if ($start >= $end)
                    return redirect()->back()->with(['error' => "La fecha de inicio tiene que ser mayor que la de fin: "]);
            $votes->save();
            return redirect()->back()->with(['message' => "Nueva votación " .  $votes->vote_name . " creada correctamente"]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "No se pudo crear la votación: " . $e->getMessage()]);
        }
    }


    public function getDataVotes()
    {
        return Votes::all()->toJson();

    }

    public function create()
    {
        $users = User::all('email', 'full_name');
        return view('content.admin.create', ['users' => $users]);
    }




    public function destroy_vote($id_vote)
    {
        try {
            $vote = Votes::find(intval($id_vote));

            $vote->delete();
            $user_vote = User_votes::where('id', intval($id_vote))->first();
            if (isset($user_vote))
                $user_vote->delete();

            $result = Votes_result::where('id', intval($id_vote))->first();
            if (isset($result))
                $result->delete();


            return redirect()->back()->with(['message' => "Votación elimnada correctamente"]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "Se produjo un error eliminando la votación: " . $e->getMessage()]);
        }
    }





    public function show($vote_id) {
        $this->workOutResults($vote_id);
        $results = $this->getResults($vote_id);
        return view('content.show_results', ['data' => $results]);
    }


}
