<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Votes;

use App\User_votes;
use App\Votes_result;
use Illuminate\Support\Facades\Auth;

use App\Traits\ResultTrait;


use App\Http\Requests;


class VotesController extends Controller
{

    use ResultTrait;
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    function index()
    {
        try {
            $votes = Votes::orderBy('id', 'desc')->get();
            $data_votes = "";
            $email =  Auth::user()->email;
            foreach ($votes as $vote) {
                $data_votes['candidates_info'][] = $this->getDataCandidates($vote->candidates);
                $data_votes['vote'][] = $vote;
                $user_vote = User_votes::where('id_vote', $vote->id)->where('id_user', $email)->get()->first();
                isset($user_vote) ? $data_votes['voted'][] = true :  $data_votes['voted'][] = false;
            }

            //$votaciones  = collect($data_votes);
            return view('content.index', ['votaciones' => $data_votes]);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "Error, no se pudieron obtener las votaciones. Contacte administrador" . $e->getMessage()]);
        }
    }


    public function edit($vote_id)
    {
        try {
            $email =  Auth::user()->email;
            $user_vote = User_votes::where('id_vote', $vote_id)
                ->where('id_user', $email)->get()->first();
            if ( isset($user_vote))
                return redirect()->back()->with(['error' => "Ya has votado en esta votación"]);

            $vote = Votes::find(intval($vote_id));

            $data_votes = $this->getDataCandidates($vote->candidates);
            return view('content.create_user_vote', ["vote" => $vote, 'candidates' => $data_votes ]);

        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "Error realizando la votación" . $e->getMessage()]);
        }

    }

    private function getDataCandidates($candidates)
    {

        $emails = explode(",", $candidates);

        $output = array();
        foreach ($emails as $email) {
            $user = User::where('email', $email)->get()[0];
            $output[] = ['email' => $email, 'full_name' => $user->full_name, "dni" => $user->dni];
        }

        return $output;

    }

    public function postVote(Request $request)
    {
        try {

            $vote_id = $request->input('vote_id');
            $email =  Auth::user()->email;
            $user_vote = User_votes::where('id_vote', $vote_id)
                        ->where('id_user', $email)->get()->first();

            $status = Votes::find($vote_id)->status;
            if ( $status == 0 )
                return redirect('/')->with(['error' => "No se puede votar, votación no "]);
            else if ( $status == 2 )
                return redirect('/')->with(['error' => "No se puede votar, votación terminada"]);

            if ( isset($user_vote))
                return redirect('/')->with(['error' => "Ya has votado en esta votación"]);

            $candidates = $request->input('submit_candidates');
            $user_votes = new User_votes();
            $user_votes->id_vote = $vote_id;
            $user_votes->id_user = $email;
            $user_votes->user_vote_list = implode(',', $candidates);

            $user_votes->save();
            return redirect('/')->with(["message" => "Voto realizado correctamente"]);

        } catch (\Exception $e) {
            exit;
            return redirect('/')->with(['error' => "Error realizando la votación" . $e->getMessage()]);
        }
    }



    public function show($vote_id)
    {
        try {
            $vote = Votes::find($vote_id);
            if ( $vote && $vote->status == 2) {
                $results = $this->getResults($vote_id);
                return view('content.show_results', ['data' => $results]);
            }
            else {
                return redirect()->back()->with(['error' => "La votación no ha finalizado"]);
            }

        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => "Error mostrando resultados de la votación. Contacte administrador" . $e->getMessage()]);
        }

    }

}
