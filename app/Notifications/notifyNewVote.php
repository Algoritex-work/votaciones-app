<?php

namespace App\Notifications;

use App\Votes;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Log;

class notifyNewVote extends Notification implements ShouldQueue
{
    use Queueable;
    var $vote = null;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($vote)
    {
        $this->vote = $vote;
        Log::debug("__construct". $this->vote->vote_name);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Log::debug("entra en toMail");

        $url = url('votaciones');
        return (new MailMessage)
            ->subject('Votación: ' . $this->vote->vote_name . " activada")
            ->greeting('Hola')
            ->line('Ya puedes votar en la votación ' .  $this->vote->vote_name)
            ->line('Gracias')
             ->action('Accede a Oepb Votaciones App Estibadores', $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
