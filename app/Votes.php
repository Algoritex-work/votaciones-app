<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Votes extends Model
{
    //
    use Notifiable;

    protected $table = 'votes';

    public function user_votes()
    {
        return $this->hasMany('App\User_votes','id_vote');
    }

    public function results()
    {
        return $this->hasMany('App\Votes_result', 'id_vote');
    }
}
