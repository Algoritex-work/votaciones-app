<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Votes;
use App\Http\Requests;
use App\Traits\ResultTrait;

use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    use ResultTrait;

    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {

            $votes = Votes::all();
            foreach ($votes as $vote) {
                if ($this->checkIfFStarted($vote)) {
                    Log::debug(" >> Comenzada votación: " . $vote->id . " nombre: " .$vote->vote_name);
                    $this->publish($vote->id);
                } else {
                    Log::debug("No Comenzada: " . $vote->id . " nombre: " .$vote->vote_name);
                }

                if ($this->checkIfFinished($vote)) {
                    Log::debug(">> Terminada Votación: " . $vote->id . " nombre: " .$vote->vote_name);
                    //$this->finishVote($vote->id);
                } else {
                    Log::debug("No Terminada: " . $vote->id . " nombre: " .$vote->vote_name);
                }
            }
        })->everyMinute();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
